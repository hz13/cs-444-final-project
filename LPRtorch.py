from concurrent.futures.thread import _worker
from random import shuffle
import sys
import os
import numpy as np
import torch
from torch import nn
import cv2
from torch.utils.data import Dataset, DataLoader

CHARS = ['京', '沪', '津', '渝', '冀', '晋', '蒙', '辽', '吉', '黑',
         '苏', '浙', '皖', '闽', '赣', '鲁', '豫', '鄂', '湘', '粤',
         '桂', '琼', '川', '贵', '云', '藏', '陕', '甘', '青', '宁',
         '新',
         '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
         'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
         'W', 'X', 'Y', 'Z','_'
         ]
dict = {'A01':'京','A02':'津','A03':'沪','B02':'蒙',
        'S01':'皖','S02':'闽','S03':'粤','S04':'甘',
        'S05': '贵', 'S06': '鄂', 'S07': '冀', 'S08': '黑', 'S09': '湘',
        'S10': '豫', 'S12': '吉', 'S13': '苏', 'S14': '赣', 'S15': '辽',
        'S17': '川', 'S18': '鲁', 'S22': '浙',
        'S30':'渝', 'S31':'晋', 'S32':'桂', 'S33':'琼', 'S34':'云', 'S35':'藏',
        'S36':'陕','S37':'青', 'S38':'宁', 'S39':'新'}

CHARS_DICT = {char:i for i, char in enumerate(CHARS)}

NUM_CHARS = len(CHARS)

def encode_label(s):
    label = np.zeros([len(s)])
    for i, c in enumerate(s):
        label[i] = CHARS_DICT[c]
    return label

class LPRDataset(Dataset):
    def __init__(self, image, labels):
        self.labels = labels
        self.image = image
    def __len__(self):
        return self.labels.shape
    def __getitem__(self, idx):
        label = self.labels[idx]
        image = self.image[idx]
        sample = {"Image": image, "label": label}
        return sample


class SmallBasicBlock(nn.Module):
    def __init__(self, ch_in, ch_out):
        super(SmallBasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(ch_in, ch_out/4, 1, stride=1)
        self.conv2 = nn.Conv2d(ch_out/4, ch_out/4, (3, 1), stride=1, padding=(1, 0))
        self.conv3 = nn.Conv2d(ch_out/4, ch_out/4, (1, 3), stride=1, padding=(0, 1))
        self.conv4 = nn.Conv2d(ch_out/4, ch_out, 1, stride=1)
    def forward(self, feats):
        feats = self.conv1(feats)
        feats = self.conv2(feats)
        feats = self.conv3(feats)
        feats = self.conv4(feats)
        return feats

class LPRNet(nn.Module):
    def __init__(self, num_channels=3):
        super(LPRNet, self).__init__()
        self.conv1 = nn.Conv2d(num_channels, 64, 3, stride=1, padding=1)
        self.maxpool1 = nn.MaxPool2d(3, stride=1, padding=1)
        self.maxpool2 = nn.MaxPool2d(3, stride=(2, 1), padding=(13,1))
        self.maxpool3 = nn.MaxPool2d(3, stride=(2, 1), padding=(13,1))
        self.sbb1 = SmallBasicBlock(64, 64)
        self.sbb2 = SmallBasicBlock(64, 256)
        self.sbb3 = SmallBasicBlock(256, 64)
        self.dropout = nn.Dropout()
        self.batchnorm1 = nn.BatchNorm2d(64)
        self.batchnorm2 = nn.BatchNorm2d(256)
        self.relu = nn.ReLU()
        # change kernel size from 4, 1 to 3, 1 for shape preservation!
        self.conv2 = nn.Conv2d(64, 256, (3, 1), stride=1, padding=(1, 0))
        self.conv3 = nn.Conv2d(256, NUM_CHARS+1, (1, 13), stride=1, padding=(0, 6))
        self.avgpool1 = nn.AvgPool2d((4, 1), stride=(4, 1), padding=(36,0))
        self.avgpool2 = nn.AvgPool2d((4, 1), stride=(4, 1), padding=(36,0))
        self.avgpool3 = nn.AvgPool2d((2, 1), stride=(2, 1), padding=(13,0))
        # final convolution layer.
        self.conv4 = nn.Conv2d(NUM_CHARS+1+3+64+64, NUM_CHARS+1, 1)
        # TODO: investigate this part! Shape will be N * C * T, need to change to T N C
        self.fc1 = nn.Linear((NUM_CHARS+1) * 24 * 94, (NUM_CHARS+1) * 60)

    def forward(self, input):
        # Feed forward networks
        x = input
        x = self.conv1(x)
        x = self.batchnorm1(x)
        x = self.relu(x)
        x = self.maxpool1(x)
        x = self.sbb1(x)
        x2 = x
        x = self.batchnorm1(x)
        x = self.relu(x)
        x = self.maxpool2(x)
        x = self.sbb2(x)
        x = self.batchnorm2(x)
        x = self.relu(x)
        x = self.sbb3(x)
        x3 = x
        x = self.batchnorm2(x)
        x = self.relu(x)
        x = self.maxpool3(x)
        x = self.dropout(x)
        x = self.conv2(x)
        x = self.dropout(x)
        x = self.batchnorm2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.relu(x)

        # now all elements input, x, x2, x3 has the same shape, we could do concat now!
        x1 = self.avgpool1(input)
        x2 = self.avgpool2(x2)
        x3 = self.avgpool3(x3)
        middle_feats = torch.cat([x, x1, x2, x3], dim=1)
        feats = self.conv4(middle_feats).reshape(middle_feats.shape[0],-1)
        logits = self.fc1(feats)
        logits = logits.reshape(logits.shape[0], NUM_CHARS+1, -1)
        # re-order the dims
        # the smallest dimension batch size goes into the middle, then the time series go first, then finally the number of classes.
        logits = torch.permute(logits, (1, 0, 2))
        return logits

def train(train_dir, batch_size=32, lr=0.001, noise=0.001, epochs=300):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    net = LPRNet()
    net.train()
    optimizer = nn.Adam(net.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2000, gamma=0.9)
    ctc_loss = nn.CTCLoss()
    final_x = torch.zeros(len(os.path(train_dir)), 3, 24, 94).float()
    final_y = torch.zeros(len(os.path(train_dir)), 7)
    # Load images.
    for idx, file in enumerate(os.path(train_dir)):
        # try image reshape, might not work for other shapes. (H=24, W=94)
        with open(f'{train_dir}/{file}', 'r') as f:
            img_before = cv2.imread(f)
            img_after = cv2.resize(img_before, (94, 24))
            final_x[idx, :, :, :] = img_after

        if '\u4e00' <= file[0] <= '\u9fff':
            label = file[:7]
        else:
            label = dict[file[:3]] + file[4:10]
        label = encode_label(label)
        final_y[idx, :] = label

    dataset = LPRDataset(final_x, final_y)
    train_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=2)
    for epoch in range(epochs):
        for i, (images, labels) in enumerate(train_loader):
            images, labels = images.to(device), labels.to(device)
            optimizer.zero_grad()
            logits = net.forward(images)
            probs = nn.functional.log_softmax(logits, dim=2)
            T = 60
            loss = ctc_loss(probs, labels, T, 7, reduction='None')
            loss.backward()
            optimizer.step()
        scheduler.step()

    return 

def evaluate():
    "needs to deal with beam search."
    
    return 



def data_preprocessing(train_dir, batch_size):
    
    return

 
def __main__():
    '''
    python3 LPRtorch.py test_dir batch_size lr noise 
    '''
    args = sys.argv
    if not args:
        print("do better next time!")
        return 0
    dir = args[0]
    train(dir)
    return 1
    








